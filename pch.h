#include <filesystem>
#include <iostream>
#include <memory>
#include <string>
#include <vector>
#include <string>
#include <fstream>
#include <atomic>
#include <chrono>
#include <condition_variable>
#include <cctype>
#include <thread>
#include <mutex>
#include <stdexcept>
#include <limits>
#include <cassert>
#include <errno.h>
#include <cstring>



namespace fs = std::filesystem;
#ifdef __linux
  #define _T(str) str
#else
  #define _T(str) L ## str
#endif
