#include "wave.h"

wave_t::wave_t():
  riff_(),
  fmt_hdr_(),
  fmt_(),
  data_(),
  wave_()
{
  fmt_.format_tag_= 0;
}

bool wave_t::open( const string_t & filename )
{
  fmt_.format_tag_= 0;

  std::ifstream file( filename.c_str(), std::ios_base::binary | std::ios_base::in );
  if( file.is_open() == false )
  {
    std_cout<< _T("It can not open ") << filename << std::endl;
    return false;
  }

  file.read((char*)&riff_, RIFF_SIZE );
  if(!file)
  {
    std_cout << filename << _T(": Corrupted File!") << std::endl;
    return false;
  }

  if(riff_.riff_id_[0] != 'R' ||
     riff_.riff_id_[1] != 'I' ||
     riff_.riff_id_[2] != 'F' ||
     riff_.riff_id_[3] != 'F' )
  {
    std_cout << filename << _T(": Not supported!") << std::endl;
    return false;
  }

  if(riff_.riff_fmt_[0] != 'W' ||
     riff_.riff_fmt_[1] != 'A' ||
     riff_.riff_fmt_[2] != 'V' ||
     riff_.riff_fmt_[3] != 'E' )
  {
    std_cout << filename << _T(": Not supported format!") << std::endl;
    return false;
  }

  file.read((char*)&fmt_hdr_, FMTHDR_SIZE );
  if(!file)
  {
    std_cout << filename << _T(": Corrupted File!") << std::endl;
    return false;
  }

  if(fmt_hdr_.fmt_id_[0] != 'f' ||
     fmt_hdr_.fmt_id_[1] != 'm' ||
     fmt_hdr_.fmt_id_[2] != 't' )
  {
    std_cout << filename << _T(": Not supported format id!") << std::endl;
    return false;
  }


  file.read((char*)&fmt_, FMT_SIZE );
  if(!file)
  {
    std_cout << filename << _T(": Corrupted File!") << std::endl;
    return false;
  }

  if(fmt_.channels_ != 1 && fmt_.channels_ != 2)
  {
    std_cout << filename << _T(": Invalid channel count!") << fmt_.channels_ << std::endl;
    return false;
  }

  if(fmt_.bits_per_sample_== 0)
  {
    std_cout << filename << _T(": Invalid bits per sample!") << fmt_.bits_per_sample_ << std::endl;
    return false;
  }

  unsigned fmt_extra_bytes= fmt_hdr_.fmt_size_ - FMT_SIZE;
  if(fmt_extra_bytes > 0)
  {
    if( fmt_.format_tag_ != 1)
    {
      int16_t extra_param_length= 0;
      file.read((char*)&extra_param_length, 2 ); //2 bytes
      if(!file)
      {
        std_cout << filename << _T(": Corrupted File! Extra Param") << std::endl;
        return false;
      }

      if( extra_param_length > 0 )
      {
        file.seekg(extra_param_length, std::ifstream::cur);
      }
    }
    else
    {
      file.seekg(fmt_extra_bytes, std::ifstream::cur);
    }
  }

  int cnt= 0;
  do{
    file.read((char*)&data_.data_id_, 4 );
    if(!file)
    {
      std_cout << filename << _T(": Corrupted File!") << std::endl;
      return false;
    }

    if(data_.data_id_[0] == 'd' &&
       data_.data_id_[1] == 'a' &&
       data_.data_id_[2] == 't' &&
       data_.data_id_[3] == 'a' )
      break;

    file.seekg(-3, std::ifstream::cur);

    ++cnt;
  }while(cnt < 128);


  if(data_.data_id_[0] != 'd' ||
     data_.data_id_[1] != 'a' ||
     data_.data_id_[2] != 't' ||
     data_.data_id_[3] != 'a' )
  {
    std_cout << filename << _T(": Not supported!") << std::endl;
    return false;
  }

  file.read((char*)&data_.data_size_, 4 );

  wave_.reset( new char[data_.data_size_]);
  file.read(wave_.get(), data_.data_size_ );
  if(file.bad() || file.gcount() != data_.data_size_)
    return false;

  return true;
}

wave_t::~wave_t()
{
}


int16_t wave_t::get_channels() const
{
  return fmt_.channels_;
}

int32_t wave_t::get_samples_per_sec() const
{
  return fmt_.samples_per_sec_;
}

int32_t wave_t::get_avg_bytes_per_sec() const
{
  return fmt_.avg_bytes_per_sec_;
}

int32_t wave_t::get_data_size() const
{
  return data_.data_size_;
}

uint32_t wave_t::get_sample_count() const
{
  return data_.data_size_ / (fmt_.channels_ * ((fmt_.bits_per_sample_ + 7) / 8));
}

char * wave_t::get_samples( unsigned int offset, unsigned int size, unsigned int & real_size) const
{
  real_size= 0;
  if( offset > ( unsigned )data_.data_size_ )
    return nullptr;

  real_size = ( offset + size ) < ( unsigned )data_.data_size_ ? size : ( unsigned )data_.data_size_ - offset;

  return wave_.get() + offset;
}

