#include "entity.h"

entity_t::entity_t():
  src_filename_(),
  dst_filename_(),
  wave_(new wave_t),
  dst_buffer_(),
  dst_buff_size_(),
  dst_file_size_()
{
}

bool entity_t::read(const string_t &src_filename, const string_t &dst_filename)
{
  if(!fs::exists(src_filename))
  {
    std_cout << src_filename << _T("does not exists!") << std::endl;
    return false;
  }

  if(fs::exists(dst_filename) && !fs::remove(dst_filename))
  {
    std_cout << dst_filename << _T(" exists and it can be removed not exists!") << std::endl;
    return false;
  }

  src_filename_= src_filename;
  dst_filename_= dst_filename;

  return wave_->open(src_filename);
}

bool entity_t::process()
{
  auto lame= lame_init(); /* initialize libmp3lame */
  if (lame == nullptr)
  {
    std_cout << _T("fatal error during initialization") << std::endl;
    return false;
  }

  //Initialize lame
  {
    lame_set_num_samples(lame, wave_->get_sample_count());

    if( lame_set_num_channels(lame, wave_->get_channels()) == -1)
    {
      std_cout << _T("Unsupported number of channels: ") << wave_->get_channels() << std::endl;
      return false;
    }

    if(wave_->get_channels() == 1)
      lame_set_mode( lame, MONO);

    if( lame_set_in_samplerate(lame, wave_->get_samples_per_sec()) == -1)
    {
      std_cout << _T("Unsupported sample rate: ") << wave_->get_samples_per_sec() << std::endl;
      return false;
    }

    if( lame_set_brate(lame, wave_->get_avg_bytes_per_sec()) == -1)
    {
      std_cout << _T("Unsupported avg byte rate: ") << wave_->get_avg_bytes_per_sec() << std::endl;
      return false;
    }

    lame_set_VBR( lame, vbr_default );
    lame_set_VBR_q(lame, 5 /* good */);

    /* turn off automatic writing of ID3 tag data into mp3 stream
     * we have to call it before 'lame_init_params', because that
     * function would spit out ID3v2 tag data.
     */
    lame_set_write_id3tag_automatic(lame, 0);

    /* Now that all the options are set, lame needs to analyze them and
     * set some more internal options and check for problems
     */
    if (lame_init_params(lame) < 0)
    {
      std_cout << _T("fatal error during initialization") << std::endl;
      return false;
    }
  }

  dst_file_size_= dst_buff_size_= fs::file_size(src_filename_);
  dst_buffer_.reset(new char[dst_buff_size_]);

  auto result= encoder_loop(lame);

  assert( dst_file_size_ < dst_buff_size_ );

  lame_close(lame);

  return result;
}

void entity_t::write()
{
  std::ofstream file(dst_filename_, std::ofstream::binary | std::ofstream::out);
  if(file.is_open())
  {
    file.write(dst_buffer_.get(), dst_file_size_);
    file.close();

    dst_buff_size_= 0;
    dst_buffer_.reset();
  }
}

bool entity_t::encoder_loop(lame_t lame)
{
  const int FIXED_SIZE = 8192;

  unsigned channels= wave_->get_channels();

  unsigned read_offset = 0;
  unsigned write_offset = 0;
  unsigned char * mp3_buffer= (unsigned char*)dst_buffer_.get() + write_offset;

  while( true )
  {
    int k = ( channels == 1 ) ? 1 : 2;

    unsigned size = FIXED_SIZE * k * sizeof(int16_t);

    unsigned int read_bytes= 0;
    auto pcm_buffer= wave_->get_samples(read_offset, size, read_bytes);
    if(pcm_buffer == nullptr || read_bytes == 0)
      break;
    read_offset += read_bytes;

    int write = 0;
    if( read_bytes < size )
    {
      write= lame_encode_flush(lame, mp3_buffer, dst_buff_size_ - write_offset );

      if(write > 0 && (write_offset + write) <= dst_buff_size_)
      {
        write_offset+= write;
        mp3_buffer= (unsigned char*)dst_buffer_.get() + write_offset;
      }
      else
      {
        assert(false);
      }
      break;
    }
    else
    {
      unsigned read_shorts = read_bytes / 2;  // need number of 'short int' read

      if( channels == 1 )
      {
        write= lame_encode_buffer(lame, (short int*)pcm_buffer, NULL,
                                  read_shorts, mp3_buffer, dst_buff_size_ - write_offset );
      }
      else
      {
        write= lame_encode_buffer_interleaved(lame, (short int*)pcm_buffer,
                                              read_shorts/2, mp3_buffer, dst_buff_size_ - write_offset);
      }

      if(write > 0 && (write_offset + write) <= dst_buff_size_)
      {
        write_offset+= write;
        mp3_buffer= (unsigned char*)dst_buffer_.get() + write_offset;
      }
    }
  }

  dst_file_size_= write_offset;

  return true;
}

