#include "entity.h"

using namespace std::chrono_literals;
class entities_t{
public:
  entities_t():
    cond_(),
    entities_mtx_(),
    entities_()
  {}

  void add(const auto_entity_t & entity)
  {
    {
      std::lock_guard<std::mutex> lock(entities_mtx_);
      entities_.push_back(entity);
    }

    cond_.notify_all();
  }

  auto_entity_t get()
  {
    auto_entity_t entity= nullptr;

    std::unique_lock<std::mutex> lock(entities_mtx_);
    if(cond_.wait_for(lock, 4ms, [&]{
                      return !entities_.empty();
                      }))
    {
      if(!entities_.empty())
      {
        entity= *entities_.rbegin();
        entities_.pop_back();
      }
    }

    return entity;
  }

private:
  std::condition_variable cond_;
  std::mutex entities_mtx_;
  std::vector<auto_entity_t> entities_;
};
using auto_entities_t= std::shared_ptr<entities_t>;

auto_entities_t process_entities_(new entities_t());
auto_entities_t write_entities_(new entities_t());

std::atomic_int waiting_for_process_count_= 0;
std::atomic_int processed_count_= 0;
std::atomic_bool processing_finished_= false;

void read_trunc(const std::vector<std::pair<string_t, string_t>> &filenames)
{
  auto count(0);
  for(auto it: filenames)
  {
    auto_entity_t ent(new entity_t);
    if(ent->read(it.first, it.second))
    {
      process_entities_->add(ent);
      ++count;
    }
  }

  waiting_for_process_count_= count;
  return;
}

void process_trunc()
{
  while(processed_count_ < waiting_for_process_count_)
  {
    auto entity= process_entities_->get();
    if(entity == nullptr)
      continue;

    if(entity->process())
      write_entities_->add(entity);

    ++processed_count_;
  }
  return;
}

void write_trunc()
{
  while(!processing_finished_)
  {
    if(auto entity= write_entities_->get(); entity != nullptr)
    {
      entity->write();
      entity.reset();
    }
  }

  while(1)
  {
    auto entity= write_entities_->get();
    if(entity == nullptr)
      break;

    entity->write();
    entity.reset();
  }
  return;
}

void process_files(const std::vector<std::pair<string_t, string_t>> &filenames)
{
  if(filenames.empty())
    return;

  auto file_count= filenames.size();
  waiting_for_process_count_= file_count;

  uint32_t process_thread_count= 8;

  //It may return 0 when not able to detect
  auto cpu_count = std::thread::hardware_concurrency();
  if(cpu_count > 2)
    process_thread_count= cpu_count - 2;

  if(process_thread_count > file_count)
    process_thread_count = file_count;

  //To debug
  //process_thread_count= 1;

  std::thread read_thread(read_trunc, filenames);
  std::thread write_thread(write_trunc);

  std::vector<std::thread> process_threads(process_thread_count);
  for(auto i(0u); i < process_thread_count ; ++i)
    process_threads[i]= std::thread(process_trunc);

  processing_finished_= false;
  read_thread.join();
  for(auto i(0u); i < process_thread_count ; ++i)
    process_threads[i].join();
  processing_finished_= true;

  write_thread.join();

  process_entities_.reset();
  write_entities_.reset();
}
