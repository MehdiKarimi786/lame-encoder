#ifndef LIB_WAVE_WAVE_H
#define LIB_WAVE_WAVE_H

class wave_t
{
public:
  wave_t();
  virtual ~wave_t();

  bool open( const string_t & filename );
  int16_t get_channels() const;
  int32_t get_samples_per_sec() const;
  int32_t get_avg_bytes_per_sec() const;
  int32_t get_data_size() const;
  uint32_t get_sample_count() const;

  char * get_samples(unsigned int offset, unsigned int size, unsigned int & real_size) const;

  struct riff_t
  {
    char riff_id_[4];
    int32_t riff_size_;
    char riff_fmt_[4];
  };

  struct fmt_hdr_t
  {
    char fmt_id_[4];
    int32_t fmt_size_;
  };

  struct fmt_t
  {
    int16_t format_tag_;
    int16_t channels_;
    int32_t samples_per_sec_;
    int32_t avg_bytes_per_sec_;
    int16_t block_align_;
    int16_t bits_per_sample_;
  };

  struct data_t
  {
    char data_id_[4];
    uint32_t data_size_;
  };

public:
  static const int RIFF_SIZE      = 12;
  static const int FMTHDR_SIZE    = 8;
  static const int FMT_SIZE       = 16;
  static const int DATA_SIZE      = 8;

private:
  riff_t riff_;
  fmt_hdr_t fmt_hdr_;
  fmt_t fmt_;
  data_t data_;
  std::unique_ptr<char[]> wave_;
};

#endif // LIB_WAVE_WAVE_H
