#include <cinemo-config.h>

extern void process_files(const std::vector<std::pair<string_t, string_t>> &filenames);

string_t to_lower(const string_t &str)
{
  string_t str_lw(str);

  std::transform(str_lw.begin(), str_lw.end(), str_lw.begin(),
                 [](string_t::value_type c){ return std::tolower(c); });

  return str_lw;
}

string_t get_dest_file(const string_t &filename)
{
  string_t dst_filename=_T("");
  auto src_path{fs::path(filename)};
  string_t ext= to_lower(src_path.extension());
  if(ext == _T(".wav"))
  {
    string_t parent_dir{src_path.parent_path()};
    string_t src_filename= src_path.filename();
    dst_filename= parent_dir + _T("/") + src_filename.substr(0, src_filename.size() - 4) + _T(".mp3");
  }

  return dst_filename;
}


int main(int argc, string_t::value_type * argv[])
{
    if (argc < 2)
    {
      // report version
      std_cout << argv[0] << _T(" Version ") << CINEMO_VERSION_MAJOR << _T(".")
                << CINEMO_VERSION_MINOR << std::endl;
      std_cout << _T("Usage: ") << argv[0] << _T(" [Path to wave file or directory]") << std::endl;
      return 1;
    }

    string_t path= argv[1];

    if(!fs::exists(path))
    {
      std_cout << _T("The path does not exists!") << std::endl;
      return 1;
    }

    std::vector<std::pair<string_t, string_t>> filenames;
    if(!fs::is_directory(path))
    {
      auto dst_filename= get_dest_file(path);
      if(!dst_filename.empty())
        filenames.push_back(std::make_pair(path, dst_filename));
    }
    else
    {
      for(auto& p: fs::directory_iterator(path))
      {
        if(p.is_directory())
          continue;

        auto dst_filename= get_dest_file(p.path());
        if(!dst_filename.empty())
          filenames.push_back(std::make_pair(p.path(), dst_filename));
      }
    }

    if(filenames.empty())
    {
      std_cout << _T("The path does not have any wave file!") << std::endl;
      return 1;
    }

    process_files(filenames);

    return 0;
}
