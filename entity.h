#pragma once
#include <lame/lame.h>
#include "wave.h"

class entity_t{
public:
  entity_t();
  bool read(const string_t & src_filename, const string_t & dst_filename);
  bool process();
  void write();

private:
  bool encoder_loop(lame_t lame);

public:
  string_t src_filename_;
  string_t dst_filename_;
  std::unique_ptr<wave_t> wave_;
  std::unique_ptr<char[]> dst_buffer_;
  size_t dst_buff_size_;
  size_t dst_file_size_;

};
using auto_entity_t= std::shared_ptr<entity_t>;

